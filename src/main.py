# -*- coding: utf-8 -*-
# File: main.py
# Project: ControlPanelv3
# Created Date: Monday, December 11th 2017, 12:37:54 pm
# Author: JGReyes
# e-mail: josegpuntoreyes@gmail.com
# web: www.circuiteando.net
#
# ControlPanelv3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# ControlPanelv3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ControlPanelv3.  If not, see <http: //www.gnu.org/licenses/>.
#
# Copyright (c) 2017 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#

""" Punto de entrada del programa.

"""


import sys
from PySide import QtGui
import frm_main

if __name__ == '__main__':
    APP = QtGui.QApplication(sys.argv)
    FORM = frm_main.MainDialog()
    FORM.centrar_pantalla()
    FORM.show()
    sys.exit(APP.exec_())
