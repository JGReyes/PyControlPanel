# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'captura.ui'
#
# Created: Thu Feb  1 10:45:06 2018
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_captura(object):
    def setupUi(self, captura):
        captura.setObjectName("captura")
        captura.resize(535, 290)
        captura.setMinimumSize(QtCore.QSize(535, 290))
        captura.setMaximumSize(QtCore.QSize(535, 290))
        captura.setFocusPolicy(QtCore.Qt.StrongFocus)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/images/img/icono.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        captura.setWindowIcon(icon)
        captura.setModal(False)
        self.lblComando = QtGui.QLabel(captura)
        self.lblComando.setGeometry(QtCore.QRect(160, 20, 104, 28))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        font.setPointSize(18)
        self.lblComando.setFont(font)
        self.lblComando.setObjectName("lblComando")
        self.lblComando2 = QtGui.QLabel(captura)
        self.lblComando2.setGeometry(QtCore.QRect(290, 20, 155, 35))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        font.setPointSize(18)
        self.lblComando2.setFont(font)
        self.lblComando2.setText("")
        self.lblComando2.setScaledContents(True)
        self.lblComando2.setObjectName("lblComando2")
        self.lblTeclas = QtGui.QLabel(captura)
        self.lblTeclas.setGeometry(QtCore.QRect(10, 75, 162, 19))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        font.setPointSize(12)
        self.lblTeclas.setFont(font)
        self.lblTeclas.setObjectName("lblTeclas")
        self.lblTeclas2 = QtGui.QLabel(captura)
        self.lblTeclas2.setGeometry(QtCore.QRect(10, 120, 325, 20))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lblTeclas2.sizePolicy().hasHeightForWidth())
        self.lblTeclas2.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Consolas")
        font.setPointSize(12)
        self.lblTeclas2.setFont(font)
        self.lblTeclas2.setText("")
        self.lblTeclas2.setScaledContents(True)
        self.lblTeclas2.setObjectName("lblTeclas2")
        self.btnCapturar = QtGui.QPushButton(captura)
        self.btnCapturar.setGeometry(QtCore.QRect(10, 190, 180, 55))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        font.setPointSize(10)
        self.btnCapturar.setFont(font)
        self.btnCapturar.setObjectName("btnCapturar")
        self.btnAceptar = QtGui.QPushButton(captura)
        self.btnAceptar.setGeometry(QtCore.QRect(205, 190, 92, 55))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        font.setPointSize(10)
        self.btnAceptar.setFont(font)
        self.btnAceptar.setObjectName("btnAceptar")
        self.chkRotatorios = QtGui.QCheckBox(captura)
        self.chkRotatorios.setGeometry(QtCore.QRect(15, 265, 300, 22))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        font.setPointSize(11)
        self.chkRotatorios.setFont(font)
        self.chkRotatorios.setObjectName("chkRotatorios")
        self.lblPunto = QtGui.QLabel(captura)
        self.lblPunto.setGeometry(QtCore.QRect(345, 75, 171, 19))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        font.setPointSize(12)
        self.lblPunto.setFont(font)
        self.lblPunto.setObjectName("lblPunto")
        self.lblPunto2 = QtGui.QLabel(captura)
        self.lblPunto2.setGeometry(QtCore.QRect(345, 115, 63, 19))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        font.setPointSize(12)
        self.lblPunto2.setFont(font)
        self.lblPunto2.setObjectName("lblPunto2")
        self.gbTipo = QtGui.QGroupBox(captura)
        self.gbTipo.setGeometry(QtCore.QRect(345, 180, 140, 75))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        font.setPointSize(10)
        self.gbTipo.setFont(font)
        self.gbTipo.setFlat(True)
        self.gbTipo.setObjectName("gbTipo")
        self.cmbTipo = QtGui.QComboBox(self.gbTipo)
        self.cmbTipo.setGeometry(QtCore.QRect(3, 26, 137, 27))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.cmbTipo.setFont(font)
        self.cmbTipo.setFocusPolicy(QtCore.Qt.NoFocus)
        self.cmbTipo.setEditable(False)
        self.cmbTipo.setMaxVisibleItems(5)
        self.cmbTipo.setMaxCount(3)
        self.cmbTipo.setObjectName("cmbTipo")
        self.lblCodigo = QtGui.QLabel(captura)
        self.lblCodigo.setGeometry(QtCore.QRect(163, 158, 106, 20))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        font.setPointSize(12)
        self.lblCodigo.setFont(font)
        self.lblCodigo.setText("")
        self.lblCodigo.setObjectName("lblCodigo")
        self.lblcod = QtGui.QLabel(captura)
        self.lblcod.setGeometry(QtCore.QRect(10, 160, 146, 20))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        font.setPointSize(12)
        self.lblcod.setFont(font)
        self.lblcod.setObjectName("lblcod")

        self.retranslateUi(captura)
        QtCore.QObject.connect(self.btnCapturar, QtCore.SIGNAL("clicked()"), captura.btn_capturar_click)
        QtCore.QObject.connect(self.btnAceptar, QtCore.SIGNAL("clicked()"), captura.btn_aceptar_click)
        QtCore.QObject.connect(self.chkRotatorios, QtCore.SIGNAL("stateChanged(int)"), captura.chk_rotatorios_changed)
        QtCore.QMetaObject.connectSlotsByName(captura)

    def retranslateUi(self, captura):
        captura.setWindowTitle(QtGui.QApplication.translate("captura", "Captura de tecla", None, QtGui.QApplication.UnicodeUTF8))
        self.lblComando.setText(QtGui.QApplication.translate("captura", "Comando:", None, QtGui.QApplication.UnicodeUTF8))
        self.lblTeclas.setText(QtGui.QApplication.translate("captura", "Teclas capturadas:", None, QtGui.QApplication.UnicodeUTF8))
        self.btnCapturar.setToolTip(QtGui.QApplication.translate("captura", "Pulse el botón derecho del ratón para introducir la tecla\n"
"enter del teclado numérico.", None, QtGui.QApplication.UnicodeUTF8))
        self.btnCapturar.setText(QtGui.QApplication.translate("captura", "Iniciar/parar captura", None, QtGui.QApplication.UnicodeUTF8))
        self.btnAceptar.setText(QtGui.QApplication.translate("captura", "Aceptar", None, QtGui.QApplication.UnicodeUTF8))
        self.chkRotatorios.setToolTip(QtGui.QApplication.translate("captura", "Activando éste modo, los rotatorios de más de dos posiciones\n"
"pasan a ejecutar siempre dos teclas:\n"
"\n"
"Una cuando giran a derechas y otra cuando giran a izquierdas.", None, QtGui.QApplication.UnicodeUTF8))
        self.chkRotatorios.setText(QtGui.QApplication.translate("captura", "Rotatorios en modo avance/retroceso", None, QtGui.QApplication.UnicodeUTF8))
        self.lblPunto.setText(QtGui.QApplication.translate("captura", "Punto seleccionado:", None, QtGui.QApplication.UnicodeUTF8))
        self.lblPunto2.setText(QtGui.QApplication.translate("captura", "Ninguno", None, QtGui.QApplication.UnicodeUTF8))
        self.gbTipo.setTitle(QtGui.QApplication.translate("captura", "Tipo de acción:", None, QtGui.QApplication.UnicodeUTF8))
        self.lblcod.setText(QtGui.QApplication.translate("captura", "Código de tecla:", None, QtGui.QApplication.UnicodeUTF8))

import images_rc
