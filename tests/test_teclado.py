# -*- coding: utf-8 -*-
# File: test_teclado.py
# Project: ControlPanelv3
# Created Date: Tuesday, December 26th 2017, 4:13:33 pm
# Author: JGReyes
# e-mail: josegpuntoreyes@gmail.com
# web: www.circuiteando.net
#
# ControlPanelv3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# ControlPanelv3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ControlPanelv3.  If not, see <http: //www.gnu.org/licenses/>.
#
# Copyright (c) 2017 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#
""" Tests para teclado.py
"""

import time
import threading
import queue
import pyHook
import pythoncom
import pytest
from hypothesis import given, assume, settings
import hypothesis.strategies as st
import teclado

TECLAS_PULSADAS = queue.Queue(maxsize=150)


@pytest.fixture()
def virtual_keys_invalidas():
    """ Devuelve una tupla con las virtual keys sin asignar o reservadas.

    Microsof Virtual-Key Codes
    https://msdn.microsoft.com/es-es/library/windows/desktop/
    dd375731%28v=vs.85%29.aspx

    :return: Códigos de tecla no válidos o reservados.
    :rtype: tuple
    """
    return (0x07, 0x0A, 0x0B, 0x0E, 0x0F, 0x16, 0x1A,
            (0x3A, 0x40), 0x5E, (0x88, 0x8F), (0x97, 0x9F),
            0xB8, 0xB9, (0xC1, 0xDA), 0xE0, 0xE8, 0xFC)


@given(st.one_of(st.integers(), st.text()))
@settings(max_examples=200)
def test_tecla_no_valida(tecla):
    """ Comprueba que se devuelve False al pasar una tecla no válida.

    :param tecla: codigo de tecla a comprobar.(Generado por Hypothesis)
    """

    tec = teclado.Teclado()

    if isinstance(tecla, int):
        assume(tecla < 0x01 or tecla > 0xFE)
    assert not tec.pulsar(tecla)
    assert not tec.soltar(tecla)


def test_virtual_key_valida(virtual_keys_invalidas):
    """Comprueba que todos los códigos virtuales de la clase Teclado
     son válidos.

    :param virtual_keys_invalidas: Teclas no válidas o reservadas.
    :type virtual_keys_invalidas: tuple
    """

    tec = teclado.Teclado()

    for tecla in tec.teclas:
        assert 0x01 <= tecla <= 0xFE  # Tecla fuera de rango.
        for t_invalida in virtual_keys_invalidas:
            if isinstance(t_invalida, int):
                assert tecla != t_invalida
            else:                           # Tupla con valor (inicio, fin)
                for tecla_r in range(t_invalida[0], t_invalida[1] + 1):
                    assert tecla != tecla_r


@pytest.mark.skip(reason='Envía teclas al SO y abre la consola. No lo querrás \
                  ejecutar asiduamente')
def test_teclas():
    """
    Comprueba todas las teclas en el diccionario 'teclas'.

    Se envían todas las teclas y se interceptan los eventos de teclado
    del sistema operativo para verificar que todas se han recibido.
    """
    tec = teclado.Teclado()
    hteclado = threading.Thread(target=hook_teclado, daemon=True)
    hteclado.start()

    for tecla in tec.teclas:
        tec.pulsar(tecla)
        time.sleep(0.05)
        tec.soltar(tecla)

    assert TECLAS_PULSADAS.qsize() == len(tec.teclas)


def hook_teclado():
    """Intercepta las teclas enviadas al sistema operativo.
    """

    def on_keyboard_event(event):
        """Se ejecuta cada vez que se levanta una tecla, y almacena la tecla
        en la cola TECLAS_PULSADAS.

        :param event: Contiene información de la tecla.
        :type event: [type]
        :return: True si quiere propagar el evento, false en caso contrario.
        :rtype: bool
        """

        TECLAS_PULSADAS.put(event.ScanCode)
        return False    # Para no propagar el evento (no fuciona correctamente)
    hook = pyHook.HookManager()
    hook.KeyUp = on_keyboard_event
    hook.HookKeyboard()
    while time.clock() < 15:
        pythoncom.PumpWaitingMessages()
    hook.UnhookKeyboard()
