# -*- coding: utf-8 -*-
# File: teclado.py
# Project: ControlPanelv3
# Created Date: Monday, December 25th 2017, 5:15:25 pm
# Author: JGReyes
# e-mail: josegpuntoreyes@gmail.com
# web: www.circuiteando.net
#
# ControlPanelv3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# ControlPanelv3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ControlPanelv3.  If not, see <http: //www.gnu.org/licenses/>.
#
# Copyright (c) 2017 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#


"""
Módulo encargado de emular un teclado.

"""

import ctypes as ct

_SENDINPUT = ct.windll.user32.SendInput
_PUNTERO = ct.POINTER(ct.c_ulong)
_KEYEVENTF_SCANCODE = 0x0008
_KEYEVENTF_EXTENDEDKEY = 0x0001
_KEYEVENTF_KEYUP = 0x0002


class _KeyBdInput(ct.Structure):
    _fields_ = [("wVk", ct.c_ushort),
                ("wScan", ct.c_ushort),
                ("dwFlags", ct.c_ulong),
                ("time", ct.c_ulong),
                ("dwExtraInfo", _PUNTERO)]


class _HardwareInput(ct.Structure):
    _fields_ = [("uMsg", ct.c_ulong),
                ("wParamL", ct.c_short),
                ("wParamH", ct.c_ushort)]


class _MouseInput(ct.Structure):
    _fields_ = [("dx", ct.c_long),
                ("dy", ct.c_long),
                ("mouseData", ct.c_ulong),
                ("dwFlags", ct.c_ulong),
                ("time", ct.c_ulong),
                ("dwExtraInfo", _PUNTERO)]


class _Input_I(ct.Union):
    _fields_ = [("ki", _KeyBdInput),
                ("mi", _MouseInput),
                ("hi", _HardwareInput)]


class _Input(ct.Structure):
    _fields_ = [("type", ct.c_ulong),
                ("ii", _Input_I)]


class Teclado:
    """Clase encargada de emular un teclado.

    Manda scancodes, lo que lo hace indistinguible de una teclado real,
    siendo el único método que funciona en aplicaciones DirectX.
    """

    
    """.. py:attribute:: teclas
    Contiene los scancodes(Set 1)

    El diccionario 'teclas' contiene en la clave, los virtual keys codes 
    que identifican una tecla en el sistema operativo. El valor lo forma
    una tupla con el nombre de la tecla y el scancode (los codigos que 
    envían los drivers de los teclados al sistema operativo)."""    

    teclas = {}    
    teclas[16] = ('SHIFT', 42)
    teclas[17] = ('CTRL', 29)
    teclas[18] = ('ALT', 56)
    teclas[48] = ('0', 11)
    teclas[49] = ('1', 2)
    teclas[50] = ('2', 3)
    teclas[51] = ('3', 4)
    teclas[52] = ('4', 5)
    teclas[53] = ('5', 6)
    teclas[54] = ('6', 7)
    teclas[55] = ('7', 8)
    teclas[56] = ('8', 9)
    teclas[57] = ('9', 10)
    teclas[65] = ('A', 30)
    teclas[66] = ('B', 48)
    teclas[67] = ('C', 46)
    teclas[68] = ('D', 32)
    teclas[69] = ('E', 18)
    teclas[70] = ('F', 33)
    teclas[71] = ('G', 34)
    teclas[72] = ('H', 35)
    teclas[73] = ('I', 23)
    teclas[74] = ('J', 36)
    teclas[75] = ('K', 37)
    teclas[76] = ('L', 38)
    teclas[77] = ('M', 50)
    teclas[78] = ('N', 49)
    teclas[79] = ('O', 24)
    teclas[80] = ('P', 25)
    teclas[81] = ('Q', 16)
    teclas[82] = ('R', 19)
    teclas[83] = ('S', 31)
    teclas[84] = ('T', 20)
    teclas[85] = ('U', 22)
    teclas[86] = ('V', 47)
    teclas[87] = ('W', 17)
    teclas[88] = ('X', 45)
    teclas[89] = ('Y', 21)
    teclas[90] = ('Z', 44)
    teclas[107] = ('ADD', 78)
    teclas[8] = ('BACK', 14)
    teclas[20] = ('CAPITAL', 58)
    teclas[110] = ('DECIMAL', 83)
    teclas[46] = ('DELETE', 1211)  # Le sumo 1000 a los scancodes
                                      # expandidos.
    teclas[111] = ('DIVIDE', 1181)
    teclas[40] = ('DOWN', 1208)
    teclas[35] = ('END', 1207)
    teclas[27] = ('ESCAPE', 1)
    teclas[112] = ('F1', 59)
    teclas[113] = ('F2', 60)
    teclas[114] = ('F3', 61)
    teclas[115] = ('F4', 62)
    teclas[116] = ('F5', 63)
    teclas[117] = ('F6', 64)
    teclas[118] = ('F7', 65)
    teclas[119] = ('F8', 66)
    teclas[120] = ('F9', 67)
    teclas[121] = ('F10', 68)
    teclas[122] = ('F11', 87)
    teclas[123] = ('F12', 88)
    teclas[24] = ('FINAL', 1207)
    teclas[36] = ('HOME', 1199)
    teclas[45] = ('INSERT', 1210)
    teclas[96] = ('NUMPAD0', 82)
    teclas[97] = ('NUMPAD1', 79)
    teclas[98] = ('NUMPAD2', 80)
    teclas[99] = ('NUMPAD3', 81)
    teclas[100] = ('NUMPAD4', 75)
    teclas[101] = ('NUMPAD5', 76)
    teclas[102] = ('NUMPAD6', 77)
    teclas[103] = ('NUMPAD7', 71)
    teclas[104] = ('NUMPAD8', 72)
    teclas[105] = ('NUMPAD9', 73)
    teclas[106] = ('MULTIPLY', 55)
    teclas[32] = ('SPACE', 57)
    teclas[108] = ('SEPARATOR', 1053)
    teclas[109] = ('SUBTRACT', 74)
    teclas[9] = ('TAB', 15)
    teclas[38] = ('UP', 1200)
    teclas[39] = ('RIGHT', 1205)
    teclas[37] = ('LEFT', 1203)
    teclas[19] = ('PAUSE', 1197)
    teclas[145] = ('SCROLL', 1070)
    teclas[13] = ('RETURN', 28)
    teclas[144] = ('NUMLOCK', 1069)
    teclas[33] = ('PRIOR', 1201)
    teclas[220] = ('OEM_5', 41)
    teclas[226] = ('OEM_102', 86)
    teclas[34] = ('NEXT', 1209)
    teclas[221] = ('CLOSE_BRAKET', 13)
    teclas[188] = ('COMMA', 51)
    teclas[187] = ('EQUAL', 27)
    teclas[189] = ('MINUS', 53)
    teclas[190] = ('POINT', 52)
    teclas[222] = ('QUOTE', 40)
    teclas[186] = ('SEMI_COMMA', 26)
    teclas[219] = ('OPEN_BRAKET', 12)
    teclas[191] = ('SLASH', 43)
    teclas[192] = ('TILDE', 39)
    teclas[176] = ('NUMPAD_RETURN', 1028)     

    def _obtener_scancode(self, tecla):
        """Se encarga que obtener el scancode del código de tecla 
        proporcionado.
        
        :param tecla: Es el código de la tecla que se quiere emular.
        :type tecla: int
        :return: Nombre y scancode de la tecla pasada como parámetro.
        :rtype: tuple (Nombre, scancode)
                        De no encontrar la tecla, devuelve None
        """
   
        return Teclado.teclas.get(tecla)
  
    def pulsar(self, tecla):
        """Realiza la misma función que presionar una tecla.
        
        :param tecla: Es el código de la tecla que se quiere emular.
        :type tecla: int
        :return: True si ejecuta la acción, False si no existe la tecla.
        :rtype: bool
        """

        if tecla in Teclado.teclas:
            ii = _Input_I()
            ii.ki = _KeyBdInput()
            ii.ki.wVk = ct.c_ushort(0)
            ii.ki.wScan = ct.c_ushort(self._obtener_scancode(tecla)[1])
            ii.ki.dwFlags = _KEYEVENTF_SCANCODE
            ii.ki.time = ct.c_ulong(0)
            ii.ki.dwExtraInfo = ct.pointer(ct.c_ulong(0))

            if ii.ki.wScan > 1000:
                ii.ki.wScan = ii.ki.wScan - 1000
                ii.ki.dwFlags = (ii.ki.dwFlags | _KEYEVENTF_EXTENDEDKEY)

            entrada = _Input(ct.c_ulong(1), ii)
            _SENDINPUT(1, ct.pointer(entrada), ct.sizeof(entrada))
            return True
        else:
            return False

    def soltar(self, tecla):
        """Realiza la misma función que presionar una tecla.
        
        :param tecla: Es el código de la tecla que se quiere emular.
        :type tecla: int
        :return: True si ejecuta la acción, False si no existe la tecla.
        :rtype: bool
        """
       
        if tecla in Teclado.teclas:
            ii = _Input_I()
            ii.ki = _KeyBdInput()
            ii.ki.wVk = ct.c_ushort(0)
            ii.ki.wScan = ct.c_ushort(self._obtener_scancode(tecla)[1])
            ii.ki.dwFlags = (_KEYEVENTF_SCANCODE | _KEYEVENTF_KEYUP)
            ii.ki.time = ct.c_ulong(0)
            ii.ki.dwExtraInfo = ct.pointer(ct.c_ulong(0))

            if ii.ki.wScan > 1000:
                ii.ki.wScan = ii.ki.wScan - 1000
                ii.ki.dwFlags = (ii.ki.dwFlags | _KEYEVENTF_EXTENDEDKEY)

            entrada = _Input(ct.c_ulong(1), ii)
            _SENDINPUT(1, ct.pointer(entrada), ct.sizeof(entrada))
            return True
        else:
            return False
