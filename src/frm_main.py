# -*- coding: utf-8 -*-
# File: frm_main.py
# Project: ControlPanelv3
# Created Date: Friday, December 15th 2017, 6:30:27 pm
# Author: JGReyes
# e-mail: josegpuntoreyes@gmail.com
# web: www.circuiteando.net
#
# ControlPanelv3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# ControlPanelv3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ControlPanelv3.  If not, see <http: //www.gnu.org/licenses/>.
#
# Copyright (c) 2017 JGReyes
#
# ------------------------------------
# Last Modified: 31/10/2019
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# 31/10/19   1.01      JGReyes     Se añade posibilidad de especificar el tiempo 
#                                  entre pulsaciones en el archivo de perfil.
#                                  Ver ejemplo en perfil DCS_F18.cp3

""" Contiene el formulario principal del programa así como su lógica.
"""

from enum import Enum
import configparser
import os
from pathlib import Path, PureWindowsPath as winpath
import time
from PySide import QtGui, QtCore
import mainwindow
import frm_captura
import teclado
import cpanel
import crcmod
import winreg

class Modo(Enum):
    """ Indica el modo de funcionamiento del programa.

    - edicion: Activa el modo edición, permitiendo asirnar comandos de teclas
    a los botones/interruptores del panel.

    - prueba: En este modo, al pulsar/activar un botón/interruptor, un punto
    de color rojo se muestra en pantalla en la posición correspondiente durante
    medio segundo.

    -normal: Éste es el modo habitual de operación, en él no se muestra nada
    en pantalla, siendo el único en el que se mandan las teclas asiganadas al
    sistema operativo.
    """

    edicion = 1
    prueba = 2
    normal = 3


class MainDialog(QtGui.QMainWindow, mainwindow.Ui_MainWindow):
    """ Muestra el formulario principal y controla todo el programa.
    """

    crc16arc = crcmod.Crc(0x18005, 0, True, 0)

    def __init__(self, parent=None):
        super(MainDialog, self).__init__(parent)
        self.setupUi(self)
        # Ruta al archivo de configuración.
        self.archivo_perfil = ''
        # Indica si el modo avance / retroceso de los rotatorios está o no
        # activado.
        self.modo_av_ret_rotatorios = False
        # Contiene una copia en memoria del perfil.
        self._perfil = None
        self.mostrar_puntos(False)
        self.capturar_puntos = False
        # Modo actual en el que se encuentra la aplicación.
        self.modo_actual = Modo.normal
        # Indica si es panel está activo y puede mandar teclas.
        self.panel_activo = True
        self._pos_rot_a = 0xA0    # Posición actual del rotatorio A.
        self._pos_rot_c = 0xC0    # Posición actual del rotatorio C.
        self._pos_rot_d = 0xD0    # Posición actual del rotatorio D.
        # Tiempo en el que se capturó el último comando en modo edición.
        self.tiempo = time.clock()
        self._cp3_usb = None  # Dispositivo USB para comunicación con el panel.
        # Tiempo entre pulsación de las teclas modificadoras.
        self.tModificadoraS = 0.010 
        # Tiempo del resto de teclas al realizar una pulsación.
        self.tTeclaS = 0.100
        self._inicializar()
        self.fcaptura = frm_captura.CapturaDialog(self)
        self.installEventFilter(self)
       
    def centrar_pantalla(self):
        """ Centra el formulario principal en la pantalla.
        """
        geometria = self.frameGeometry()
        centro_pantalla = QtGui.QDesktopWidget().availableGeometry().center()
        geometria.moveCenter(centro_pantalla)
        self.move(geometria.topLeft())

    def btn_cargar_click(self):
        """ Carga un archivo de perfil o crea uno nuevo si se indica.
        """

        dialog = QtGui.QFileDialog(self,
                        caption='Selecciona un perfil o indica uno a crear')
        dialog.setFileMode(QtGui.QFileDialog.AnyFile)
        dialog.setNameFilter('Perfil ControlPanel (*.cp3)')
        dialog.setViewMode(QtGui.QFileDialog.List)
        dialog.setAcceptMode(QtGui.QFileDialog.AcceptOpen)

        if dialog.exec_():
            archivos = dialog.selectedFiles()
            nombre_archivo = archivos[0]
            if '.cp3' not in nombre_archivo:
                nombre_archivo += '.cp3'
        else:
            nombre_archivo = ''

        if nombre_archivo:
            if Path(nombre_archivo).is_file():
                self.cargar_perfil(nombre_archivo)
            else:
                mbox = QtGui.QMessageBox(QtGui.QMessageBox.Question,
                                         'El perfil no existe',
                                         '¿Desea crear un perfil nuevo?',
                                         QtGui.QMessageBox.Yes |
                                         QtGui.QMessageBox.No, self)
                rest = mbox.exec_()
                if rest == QtGui.QMessageBox.No:
                    self.lblPerfil_2.setText('Ninguno')
                else:
                    self.crear_perfil(nombre_archivo)

    def mostrar_puntos(self, mostrar=True):
        """Se encarga de mostrar y ocultar los puntos rojos en el modo de
        edicicón.

        :param mostrar: True muestra los puntos en la pantalla.
                        False quita los puntos de la pantalla
                        default: True
        :type mostrar: bool
        """

        for widget in self.Panel1.children():
            if (isinstance(widget, QtGui.QLabel) and
                    'lbl_sp' in widget.objectName()):
                if mostrar:
                    widget.installEventFilter(self)
                    widget.show()
                else:
                    widget.hide()

    def _inicializar(self):
        """ Inizializa variables y el texto de los labels.

        """

        self.modo_actual = Modo.normal
        self.lblPerfil_2.setText('Ninguno')
        self.lblCon_2.setText('Desconec.')
        self.lblCon_2.setStyleSheet('color: rgb(255, 0, 0);')
        self.lblModo_2.setText('Normal')
        self.perfil_por_def_reg(True)

    def perfil_por_def_reg(self, leer, ruta_guardado=''):
        """ Se encarga de crear o cargar la ruta al perfil por defecto.
        Es una versión alternativa a 'perfil_por_def', utiliza el registro
        de windows en vez de un archivo .ini.
        
        :param leer: si está en True, lee la ruta del perfil por defecto,
                     si está en False se crea un valor en el registro con
                     con la ruta al perfil por defecto.
        :type leer: bool
        :param ruta_guardado: contiene la ruta del perfil que se quiere cargar
                              al arrancar la aplicación. Sólo se tiene en
                              cuenta si cargar es False, puesto que se va a
                              guardar. Por defecto es cadena vacía.
        :type ruta_guardado: string
        """

        try:
            if leer:
                program_key = winreg.OpenKey(winreg.HKEY_CURRENT_USER,
                                            'Software\PyControlPanelv3')
                ruta_perfil_def, tipo_reg = winreg.QueryValueEx(program_key, 
                                                              'perfil_defecto')
                winreg.CloseKey(program_key)

                if ruta_perfil_def:
                    if Path(ruta_perfil_def).is_file():
                        self.cargar_perfil(ruta_perfil_def)

            elif ruta_guardado:
                program_key = winreg.CreateKey(winreg.HKEY_CURRENT_USER,
                                               'Software\PyControlPanelv3') 

                winreg.SetValueEx(program_key, 'perfil_defecto', 0, 
                                  winreg.REG_SZ, ruta_guardado)
                winreg.CloseKey(program_key)
        except WindowsError:
            pass  # No existe la clave en el registro.

        except Exception as ex:
            mbox = QtGui.QMessageBox(QtGui.QMessageBox.Critical,
                                     'Se ha producido un error',
                                     repr(ex), QtGui.QMessageBox.Ok, self)
            mbox.exec_()

    def perfil_por_def(self, cargar, ruta_guardado=''):
        """ Se encarga de crear o cargar un archivo .ini que contiene el perfil
        que se carga por defecto al arrancar la aplicación.

        :param cargar: si está en True, carga el archivo .ini y el perfil
                       indicado en éste, si está en False se crea o guarda el
                       archivo .ini
        :type cargar: bool
        :param ruta_guardado: contiene la ruta del perfil que se quiere cargar
                              al arrancar la aplicación. Sólo se tiene en
                              cuenta si cargar es False, puesto que se va a
                              guardar. Por defecto es cadena vacía.
        :type ruta_guardado: string
        """
       
        ruta = ''.join([os.getcwd(), '\perfil_inicio.ini'])
        try:
            initfile = configparser.ConfigParser()
            if Path(ruta).is_file() and cargar:
                initfile.read(ruta)
                ruta_perfil_def = initfile.get('PERFIL', 'Por_defecto',
                                               fallback='')
                if ruta_perfil_def:
                    self.cargar_perfil(ruta_perfil_def)

            if not cargar and ruta_guardado:
                # Se guarda el perfil como perfil por defecto
                initfile.add_section('PERFIL')
                initfile.set('PERFIL', 'Por_defecto', ruta_guardado)
                with open(ruta, 'w') as configfile:
                    initfile.write(configfile)

        except Exception as ex:
            mbox = QtGui.QMessageBox(QtGui.QMessageBox.Critical,
                                     'Se ha producido un error',
                                     repr(ex), QtGui.QMessageBox.Ok, self)
            mbox.exec_()

    def teclado_pulsar(self, comando):
        """ Se encarga de procesar el comando enviado por el dispositivo y
        pulsar la tecla correspondiente o configurarla, según el modo de
        operación que esté seleccionado.

        :param comando: contiene el botón/interruptor accionado en el
                        dispositivo.
        :type comando: string
        :return: 1 si no se ha mandado la tecla o no se ha procesado el
                 comando.
        :rtype: int
        """

        try:
            if not isinstance(self._perfil, configparser.ConfigParser):
                mbox = QtGui.QMessageBox(QtGui.QMessageBox.Critical,
                                         'Se ha producido un error',
                                         'No hay ningún perfil seleccionado',
                                         QtGui.QMessageBox.Ok, self)
                mbox.exec_()
                return 1
            else:
                if comando == '0B460':  # Pulsador del joystick analógico.
                    if self.panel_activo:
                        # Desactivo el panel para no mandar teclas.
                        self.panel_activo = False
                    else:
                        self.panel_activo = True
                        # Para no habilitar la tecla para otra función.
                        return 1

                if (not self.panel_activo or comando == '0B461' or
                        comando == '000E7'):
                    return 1

                if self.modo_actual == Modo.normal:
                    atrib_comando = self.cargar_teclas(comando)
                    if atrib_comando:
                        tipo = atrib_comando[0]
                        teclas = atrib_comando[2]
                        codigo = atrib_comando[3]
                        self.ejecutar_teclas(tipo, teclas, codigo)

                if self.modo_actual == Modo.prueba:
                    atrib_comando = self.cargar_teclas(comando)
                    if atrib_comando:
                        punto = atrib_comando[1]
                        # Muestra el punto rojo del botón/interruptor accionado
                        # durante 500 ms.
                        if punto:
                            for widget in self.Panel1.children():
                                if (isinstance(widget, QtGui.QLabel) and
                                        punto in widget.objectName()):
                                    widget.show()
                                    widget.repaint()
                                    time.sleep(0.5)
                                    widget.hide()
                                    break

                if self.modo_actual == Modo.edicion:
                    # Solo captura un comando para su edición si han pasado 80
                    # ms o más desde el anterior comando, de este modo se
                    # pueden pulsar rápidamente los botones para obtener el
                    # código asociado a la pulsación y no el asociado al soltar
                    # el botón.

                    if (time.clock() - self.tiempo) < 0.08:
                        return 1

                    self.fcaptura.close()
                    self.fcaptura.show()
                    self.fcaptura.comando = comando
                    self.fcaptura.lblComando2.setText(comando)
                    if self.modo_av_ret_rotatorios:
                        self.fcaptura.chkRotatorios.setChecked(True)
                    else:
                        self.fcaptura.chkRotatorios.setChecked(False)

                    atrib_comando = self.cargar_teclas(comando, True)
                    if not atrib_comando:
                        self.fcaptura.cmbTipo.setCurrentIndex(0)
                        self.fcaptura.lblPunto2.setText('')
                        self.fcaptura.lblTeclas2.setText('')
                        self.fcaptura.lblCodigo.setText('')
            # Actualiza el tiempo del último comando capturado.
            self.tiempo = time.clock()
        except Exception as ex:
            mbox = QtGui.QMessageBox(QtGui.QMessageBox.Critical,
                                     'Se ha producido un error',
                                     repr(ex), QtGui.QMessageBox.Ok, self)
            mbox.exec_()
            self.lblPerfil_2.setText('Ninguno')


    def cargar_teclas(self, comando, mostrar=False):
        """ Lee la configuración del comando recibido y carga las teclas que
        debe emular.
        
        :param comando: comando recibido del dispositivo.
        :type comando: string
        :param mostrar: indica si se quiere carga los datos anteriores en el 
        formulario de edición. Por defecto es False.
        :param mostrar: bool
        :return: tipo, punto, teclas y codigo del comando recibido.
                 tupla vacía en caso de error.
        :rtype: tuple
        """

        cmd_rotatorios = ('000A0', '000A1', '000A2', '000A3', '000C0', '000C1',
                          '000C2', '000C3', '000C4', '000C5', '000D0', '000D1',
                          '000D2', '000D3', '000D4')
        es_rotatorio = False
        codigo_str = ''
        valor = 0
        rot_num = 0
        comando = comando
        codigo = 0

        if self.modo_av_ret_rotatorios:
            for num, cmd in enumerate(cmd_rotatorios):
                if comando == cmd:
                    es_rotatorio = True
                    rot_num = num
                    break

            if es_rotatorio:
                # Paso el comando a número hexadecimal.
                try:
                    valor = int(comando, 16)
                except ValueError:
                    pass
                else:
                    if rot_num in range(4):   # Es el rotatorio A.
                        if valor > self._pos_rot_a:  # Giro a derechas.
                            comando = '000A1'
                        else:                     # Giro a izquierdas.
                            comando = '000A0'
                        self._pos_rot_a = valor     # Se actualiza la posición.

                    if rot_num in range(4, 10):   # Es el rotatorio C.
                        if valor > self._pos_rot_c:
                            comando = '000C1'
                        else:
                            comando = '000C0'
                        self._pos_rot_c = valor

                    if rot_num in range(10, 15):  # Es el rotatorio D.
                        if valor > self._pos_rot_d:
                            comando = '000D1'
                        else:
                            comando = '000D0'
                        self._pos_rot_d = valor

                    if self.modo_actual == Modo.edicion:
                        self.fcaptura.lblComando2.setText(comando)
                        self.fcaptura.comando = comando
        
        tipo = self._perfil.get(comando, 'tipo', fallback='')
        punto = self._perfil.get(comando, 'punto', fallback='')
        teclas = self._perfil.get(comando, 'teclas', fallback='')
        codigo_str = self._perfil.get(comando, 'codigo', fallback='')

        if not tipo:
            return ()

        if codigo_str:
            try:
                valor = int(codigo_str)
            except ValueError:
                codigo = 0
            else:
                codigo = valor
        else:
            codigo = 0

        if mostrar:
            index = self.fcaptura.cmbTipo.findText(tipo)
            self.fcaptura.cmbTipo.setCurrentIndex(index)
            self.fcaptura.lblPunto2.setText(punto)
            self.fcaptura.lblTeclas2.setText(teclas)
            self.fcaptura.lblCodigo.setText(codigo_str)

        return (tipo, punto, teclas, codigo)


    def ejecutar_teclas(self, tipo, teclas, codigo):
        """ Manda las teclas al sistema operativo mediante llamadas a su API.
        
        :param tipo: es la acción que se realiza con la tecla. Puede ser:
                    ---Pulsar: emula una pulsación rápida de la tecla.
                    ---Mantener: emula el mantener una tecla continuamente 
                       pulsada.
                    ---Soltar: emula que se suelta una tecla mantenida
                       anteriormente.
        :type tipo: string
        :param teclas: secuencia de teclas a emular separadas por el signo +
        :type teclas: string
        :param codigo: representa el código de la tecla pulsada que no es una
                       tecla de control, para emular correctamente su pulsación
                       en el sistema operativo.
        :type codigo: int
        """


        if codigo != 0:
            mi_teclado = teclado.Teclado()
            
            if 'Shift' in teclas or 'SHIFT' in teclas:
                mi_teclado.pulsar(16)
                time.sleep(self.tModificadoraS)
            if 'Ctrl' in teclas or 'CTRL' in teclas:
                mi_teclado.pulsar(17)
                time.sleep(self.tModificadoraS)
            if 'Alt' in teclas or 'ALT' in teclas:
                mi_teclado.pulsar(18)
                time.sleep(self.tModificadoraS)
            if tipo == 'Pulsar':
                mi_teclado.pulsar(codigo)
                time.sleep(self.tTeclaS)
                mi_teclado.soltar(codigo)
            if tipo == 'Mantener':
                mi_teclado.pulsar(codigo)
            if tipo == 'Soltar':
                mi_teclado.soltar(codigo)
            if 'Alt' in teclas or 'ALT' in teclas:
                mi_teclado.soltar(18)
            if 'Ctrl' in teclas or 'CTRL' in teclas:
                mi_teclado.soltar(17)
            if 'Shift' in teclas or 'SHIFT' in teclas:
                mi_teclado.soltar(16)

    def gb_conexion_toggled(self):
        """ Permine conectar y desconectar la comunicación con el dispositivo.
        """

        try:
            if self.btnOn.isChecked():
                if not self._cp3_usb:
                    self._cp3_usb = cpanel.Cpanel(self)
                    self._cp3_usb.en_cola.connect(self.comprueba_datos_usb)
                    self._cp3_usb.start()
            else:
                if self._cp3_usb:
                    self._cp3_usb.stop()
                    self._cp3_usb = None
                    self.lblCon_2.setText('Desconec.')
                    self.lblCon_2.setStyleSheet('color: rgb(255, 0, 0);')

        except Exception as ex:
            mbox = QtGui.QMessageBox(QtGui.QMessageBox.Critical,
                                     'Se ha producido un error',
                                     repr(ex), QtGui.QMessageBox.Ok, self)
            mbox.exec_()
            self.lblCon_2.setText('Desconec.')
            self.lblCon_2.setStyleSheet('color: rgb(255, 0, 0);')
            self._cp3_usb = None



    def gb_modo_toggled(self):
        """ Cambia los modos de operación del programa.
        """

        if self.btnEdicion.isChecked():
            self.modo_actual = Modo.edicion
            self.lblModo_2.setText('Edición')

        if self.btnPrueba.isChecked():
            self.modo_actual = Modo.prueba
            self.lblModo_2.setText('Prueba')

        if self.btnNormal.isChecked():
            self.modo_actual = Modo.normal
            self.lblModo_2.setText('Normal')


    @staticmethod
    def crc16_update(data, comando=True):
        """ Calcula el crc16/arc de los datos a enviar o recibidos.

        :param data: bytes de información sobre el que se calcula el crc.
        :type data: byte o bytearray
        :param comando: indica si los datos son un comando del panel.
        :type comando: bool
        :return: el valor crc de dat.
        :rtype: int
        """
      
        MainDialog.crc16arc.crcValue = 0

        if comando:
            for indice, byte in enumerate(data):
                MainDialog.crc16arc.update(bytes([data[indice]]))
            return MainDialog.crc16arc.crcValue
        else:
            MainDialog.crc16arc.update(bytes([data]))   
            return MainDialog.crc16arc.crcValue

    def cargar_perfil(self, ruta):
        """ Carga un perfil de teclas en el programa.
        
        :param ruta: ruta completa del archivo a cargar.Tiene extensión .cp3
        :type ruta: string
        """

        try:
            self._perfil = configparser.ConfigParser()
            self._perfil.read(ruta)
            modo_rotatorios = self._perfil.get('ROTATORIOS', 'Modo_av/ret',
                                               fallback='Desactivado')
            if modo_rotatorios == 'Activado':
                self.modo_av_ret_rotatorios = True
            else:
                self.modo_av_ret_rotatorios = False
 
            self.tModificadoraS = float(self._perfil.get('TIEMPO', 'Teclas_Modificadoras',
                                                   fallback='0.010'))
            self.tTeclaS = float(self._perfil.get('TIEMPO', 'Teclas_Estandar',
                                                   fallback='0.100'))                                       
            self.lblPerfil_2.setText(winpath(ruta).name[:-4])
            self.perfil_por_def_reg(False, ruta)
            self.archivo_perfil = ruta

        except Exception as ex:
            mbox = QtGui.QMessageBox(QtGui.QMessageBox.Critical,
                                     'Se ha producido un error',
                                     repr(ex), QtGui.QMessageBox.Ok, self)
            mbox.exec_()
            self.lblPerfil_2.setText('Ninguno')
            self._perfil = None
            self.archivo_perfil = ''

    def crear_perfil(self, ruta):
        """ Crea un archivo y perfil nuevo si no existe el indicado en el 
        diálogo de selección, para posteriormente poder editarlo.
        
        :param ruta: ruta completa del archivo a crear. Tiene extensión .cp3
        :type ruta: string
        """

        try:
            self._perfil = configparser.ConfigParser()
            self._perfil.add_section('ROTATORIOS')
            self._perfil.set('ROTATORIOS', 'Modo_av/ret', 'Desactivado')
            self._perfil.add_section('TIEMPO')
            self._perfil.set('TIEMPO', 'Teclas_Modificadoras', '0.010')
            self._perfil.set('TIEMPO', 'Teclas_Estandar', '0.100')
            with open(ruta, 'w') as configfile:
                self._perfil.write(configfile)
            self.cargar_perfil(ruta)

        except Exception as ex:
            mbox = QtGui.QMessageBox(QtGui.QMessageBox.Critical,
                                     'Se ha producido un error',
                                     repr(ex), QtGui.QMessageBox.Ok, self)
            mbox.exec_()
            self.lblPerfil_2.setText('Ninguno')
            self._perfil = None
            self.archivo_perfil = ''
           

    def preparar_trama(self, comando):
        """ Se encarga de preparar una trama para comunicarse con
        el dispositivo.
        
        :param comando: comando que se quiere enviar al dispositivo
        :type comando: int
        :return: Trama lista para enviar.
        :rtype: Bytearray de 6.
        """
     
        trama = bytearray(6)
        cmd = comando.to_bytes(length=3, byteorder='little')
        crc = MainDialog.crc16_update(cmd)
        crc = crc.to_bytes(length=2, byteorder='big')

        trama[0] = 0x7e         # Byte de sincronización
        trama[1] = cmd[2]       # Interruptor MSB primero 3 Bytes
        trama[2] = cmd[1]
        trama[3] = cmd[0]
        trama[4] = crc[0]       # Crc MSB
        trama[5] = crc[1]       # Crc LSB

        return trama


    def leer_trama(self, trama):
        """ Lee una trama recibida y la decodifica.
        
        :param trama: Contiene la trama a decodificar.
        :type trama: Bytearray de 6.
        :return: Número que representa el interruptor/botón accionado,
                 0 en caso de trama no válida.
        :rtype: int
        """

        if trama[0] == 0x7e:
            comando = int.from_bytes(trama[1:4], byteorder='big')
            crc_trama = int.from_bytes(trama[4:], byteorder='big')
            cmd = comando.to_bytes(length=3, byteorder='little')
            crc_calculado = MainDialog.crc16_update(cmd)

            if crc_trama == crc_calculado: #Trama válida
                return comando
            else:             # Crc erróneo
                return 0
        else:                 # Sin byte de sincronización, trama nó válida
            return 0

    @QtCore.Slot()
    def comprueba_datos_usb(self):
        """ Comprueba si hay datos recibidos por el panel.
        
        """

        comando = 0
        tx_buf = bytearray(6)
        
        if self._cp3_usb:
            trama = self._cp3_usb.salida.get() # Espera hasta recibir datos.
            if isinstance(trama, bytearray):
                tx_buf[0] = 0xaa     # Indica que se han leido los datos.
                self._cp3_usb.entrada.put(tx_buf)
                comando = self.leer_trama(trama)
               
                if not comando:
                    time.sleep(0.1)
                    trama = self.preparar_trama(0xe7)
                    # Realiza petición de reenvío.
                    self._cp3_usb.entrada.put(trama) 
                else:
                    self.teclado_pulsar('{0:05X}'.format(comando))
            else:
                if trama == 'Desconectado':
                    self.lblCon_2.setText('Desconec.')
                    self.lblCon_2.setStyleSheet('color: rgb(255, 0, 0);')

                if trama == 'Conectado':
                    self.lblCon_2.setText('Conectado')
                    self.lblCon_2.setStyleSheet('color: rgb(0, 255, 0);')
                   

    def eventFilter(self, widget, event):
        """ Captura los eventos de la aplicación para llamar a la función
        adecuada.

        :param widget: Elemento gráfico donde sucedió el evento
        :type widget: QtQui widget
        :param event: Evento que ha sucedido
        :type event: Qt event
        :return: El evento que no interesa se propaga.
        :rtype: Qt event
        """

        # Captura el nombre del punto rojo para asociarlo con el 
        # botón/interruptor correspondiente.
       
        if (event.type() == QtCore.QEvent.MouseButtonPress and
                isinstance(widget, QtGui.QLabel) and 
                'lbl_sp' in widget.objectName()):
            self.sp_mouse_click(event, widget.objectName())

        if event.type() == QtCore.QEvent.Close and widget is self:
            if self._cp3_usb:
                self._cp3_usb.stop()

        return QtGui.QWidget.eventFilter(self, widget, event)

    def sp_mouse_click(self, mouse, nombre_punto):
        """ Muestra en el formulario de captura el punto seleccionado con
        el ratón.
        
        :param mouse: evento de ratón.
        :type mouse: Qt event
        :param nombre_punto: nombre del punto seleccionado.
        :type nombre_punto: string
        """

        if (QtCore.Qt.LeftButton == mouse.button() and
                self.modo_actual == Modo.edicion and 
                self.fcaptura.isVisible()):

            self.fcaptura.lblPunto2.setText(nombre_punto[4:])
             