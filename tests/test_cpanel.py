# -*- coding: utf-8 -*-
# File: test_cpanel.py
# Project: ControlPanelv3
# Created Date: Monday, January 8th 2018, 5:07:09 pm
# Author: JGReyes
# e-mail: josegpuntoreyes@gmail.com
# web: www.circuiteando.net
#
# ControlPanelv3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# ControlPanelv3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ControlPanelv3.  If not, see <http: //www.gnu.org/licenses/>.
#
# Copyright (c) 2018 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#

""" Test para cpanel.py
"""

from time import sleep
import pytest
import cpanel


def test_conexion_usb(mocker):
    """Comprueba que se informe de la conexión y desconexión del panel USB.

    :param mocker: Emula el dispositivo hid (USB), como si estuviera
                   conectado.
    :type mocker: mock
    """

    panel = cpanel.Cpanel()
    panel.start()
    estado = panel.salida.get()
    assert estado == 'Desconectado'
    panel.stop()
    mocker.patch('cpanel.hid', autospec=True)
    panel = cpanel.Cpanel()
    panel.start()
    estado = panel.salida.get()
    assert estado == 'Conectado'
    panel.stop()


def test_cierre_conexion(mocker):
    """ Comprueba que se llama a la función encargada de cerra la conexión
    cuando se utiliza el método stop.

    :param mocker: Emula el dispositivo hid (USB), como si estuviera
                   conectado.
    :type mocker: mock
    """

    mocker.patch('cpanel.hid', autospec=True)
    panel = cpanel.Cpanel()
    panel.start()
    sleep(0.5)                      # Asegura la creación de _panel en memoria.
    panel._panel.close.return_value = True
    mocker.spy(panel._panel, 'close')
    panel.stop()
    sleep(0.5)
    assert panel._panel.close.called


def test_reconexion(mocker):
    """ Comprueba que se reintenta conectar varias veces cuando no se detecta
    el panel, y que no reintenta conectar cuando el panel está conectado.

    :param mocker: Emula el dispositivo hid (USB), como si estuviera
                   conectado.
    :type mocker: mock
    """

    panel = cpanel.Cpanel()
    mocker.spy(panel, '_conectar_panel')
    panel._conectar_panel.return_value = False
    panel.start()
    sleep(10)        # Deja tiempo para ver si reintenta conectar.
    panel._conectar_panel.return_value = True
    llamadas = panel._conectar_panel.call_count
    assert llamadas >= 5
    sleep(5)         # Para ver si reconecta cuando ya está conectado.
    panel.stop()
    assert panel._conectar_panel.call_count == llamadas


def test_enviar(mocker):
    """ Comprueba que al dejar datos en la cola de entrada, y siendo el primer
    elemento distinto de 0, se llama a la función _enviar_hid.

    :param mocker: Emula el dispositivo hid (USB), como si estuviera
                   conectado.
    :type mocker: mock
    """

    mocker.patch('cpanel.hid', autospec=True)
    panel = cpanel.Cpanel()
    panel.start()
    datos = bytearray(6)
    datos[0] = 0
    mocker.spy(panel, '_enviar_hid')
    panel.entrada.put(datos)
    sleep(0.5)
    assert not panel._enviar_hid.called
    datos[0] = 0xa1
    panel.entrada.put(datos)
    sleep(0.5)
    panel.stop()
    panel._enviar_hid.assert_called_once_with(datos)


def test_recibir(mocker):
    """ Comprueba que al recibir un report del panel, se procesa correctamente.
    Los report son de 9 bytes y los datos recibidos están entre el byte 1 y el
    6. Poniendo en la cola de salida un bytearray de 6 elementos.

    :param mocker: Emula el dispositivo hid (USB), como si estuviera
                   conectado.
    :type mocker: mock
    """

    mocker.patch('cpanel.hid', autospec=True)
    panel = cpanel.Cpanel()
    panel.start()
    report = bytearray(9)
    report[1] = 0
    report[2] = 0xb
    report[3] = 0xc
    report[4] = 0xd
    report[5] = 0xe
    report[6] = 0xf
    panel._leer_hid(report)
    sleep(0.5)
    datos = panel.salida.get_nowait()
    assert datos == 'Conectado'
    with pytest.raises(cpanel.Empty):
        datos = panel.salida.get_nowait()
    report[1] = 0xa
    panel._leer_hid(report)
    sleep(0.5)
    datos = panel.salida.get_nowait()
    assert datos == report[1:7]
    panel.stop()
