# -*- coding: utf-8 -*-
# File: test_frm_captura.py
# Project: ControlPanelv3
# Created Date: Wednesday, January 24th 2018, 4:33:17 pm
# Author: JGReyes
# e-mail: josegpuntoreyes@gmail.com
# web: www.circuiteando.net
#
# ControlPanelv3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# ControlPanelv3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ControlPanelv3.  If not, see <http: //www.gnu.org/licenses/>.
#
# Copyright (c) 2018 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#

""" Test para frm_captura.py
"""

from configparser import ConfigParser
import pytest
import frm_captura
import frm_main
from pytestqt import qtbot
from PySide import QtCore


@pytest.fixture
def captura(qtbot, mocker):
    """ Crea una instancia de frm_captura para realizar los tests.

    :param qtbot: Permite interactuar con los elementos gráficos.
    :type qtbot: pytest fixture
    :param mocker: permite acceso a funciones de mocking. 
    :type mocker: pytest-mock fixture
    :return: instancia de frm_captura
    :rtype: objeto
    """

    captura = frm_captura.CapturaDialog(None)
    main = mocker.patch('frm_main.MainDialog')
    captura.parent = main.return_value
    captura.show()
    qtbot.addWidget(captura)
    return captura


@pytest.fixture
def perfil_vacio(tmpdir):
    """ Crea un archivo de perfil con lo mínimo, sin comandos, en un
    directorio temporal.

    :param tmpdir: permite trabajar con carpetas temporales.
    :type tmpdir: pytest fixture
    :return: ruta a un archivo de perfil vacío.
    :rtype: string
    """

    config_file = tmpdir.mkdir('config').join('config.cp3')
    conf = ConfigParser()
    conf.add_section('ROTATORIOS')
    conf.set('ROTATORIOS', 'Modo_av/ret', 'Desactivado')
    with open(str(config_file), 'w') as configfile:
        conf.write(configfile)
    return str(config_file)


def test_valores_iniciales(captura, qtbot):
    """ Comprueba los valores inciales del formulario nada más mostrarse.

    :param captura: instancia de frm_captura.
    :type captura: pytest fixture
    :param qtbot: Permite interactuar con los elementos gráficos.
    :type qtbot: pytest fixture
    """

    assert not captura.parent is None
    assert captura.comando == ''
    assert not captura.capturar
    assert not captura.codigo_intro
    assert captura.codigos == []
    assert captura.cmbTipo.itemText(0) == 'Pulsar'
    assert captura.cmbTipo.itemText(1) == 'Mantener'
    assert captura.cmbTipo.itemText(2) == 'Soltar'
    assert captura.lblTeclas2.text() == ''
    assert captura.lblCodigo.text() == ''
    assert captura.lblComando2.text() == ''


def test_btn_capturar(captura, qtbot):
    """ Comprueba los cambios en el formulario después de pulsar el 
    botón de capturar.

    :param captura: instancia de frm_captura.
    :type captura: pytest fixture
    :param qtbot: Permite interactuar con los elementos gráficos.
    :type qtbot: pytest fixture
    """

    qtbot.mouseClick(captura.btnCapturar, QtCore.Qt.LeftButton)
    assert captura.capturar
    assert captura.lblTeclas2.text() == ''
    assert captura.lblCodigo.text() == ''
    assert not captura.codigo_intro
    assert captura.codigos == []
    assert captura.btnCapturar.text() == 'Capturando...'
    qtbot.mouseClick(captura.btnCapturar, QtCore.Qt.LeftButton)
    assert not captura.capturar
    assert captura.btnCapturar.text() == 'Iniciar/parar captura'


def test_eventos(captura, qtbot, mocker):
    """ Comprueba que al pulsar en el botón de captura y pulsar el teclado,
    se llama a las funciones correspondientes.

    :param captura: instancia de frm_captura.
    :type captura: pytest fixture
    :param qtbot: Permite interactuar con los elementos gráficos.
    :type qtbot: pytest fixture
    :param mocker: permite acceso a funciones de mocking. 
    :type mocker: pytest-mock fixture
    """

    mocker.spy(captura, 'btn_capturar_keypress')
    qtbot.mouseClick(captura.btnCapturar, QtCore.Qt.LeftButton)
    qtbot.keyClick(captura.btnCapturar, 'a')
    assert captura.btn_capturar_keypress.called
    mocker.spy(captura, 'btn_capturar_mouseclick')
    qtbot.mouseClick(captura.btnCapturar, QtCore.Qt.RightButton)
    assert captura.btn_capturar_mouseclick.called
    assert captura.btn_capturar_keypress.call_count == 2
    captura.close()
    captura.parent.mostrar_puntos.assert_called_with(False)
    captura.show()
    captura.parent.mostrar_puntos.assert_called_with()


def test_btn_aceptar(mocker, captura, qtbot, perfil_vacio):
    """ Comprueba que al pulsar aceptar, los datos del formulario se guardan
    en el archivo de perfil.

    :param captura: instancia de frm_captura.
    :type captura: pytest fixture
    :param qtbot: Permite interactuar con los elementos gráficos.
    :type qtbot: pytest fixture
    :param mocker: permite acceso a funciones de mocking. 
    :type mocker: pytest-mock fixture
    :param perfil_vacio: ruta a un archivo de perfil vacío.
    :type perfil_vacio: pytest fixture
    """

    mock_parent = mocker.patch('frm_main.MainDialog', autospec=True)
    mock_instance = mock_parent.return_value
    mock_instance.mostrar_puntos.return_value = None
    mock_instance.archivo_perfil = perfil_vacio
    parent = frm_main.MainDialog(None)
    captura.parent = parent
    captura.comando = '1B411'
    captura.lblTeclas2.setText('Ctrl+Alt+H')
    captura.lblCodigo.setText('72')
    captura.cmbTipo.setItemText(0, 'Pulsar')
    captura.lblPunto2.setText('sp87')
    qtbot.mouseClick(captura.btnAceptar, QtCore.Qt.LeftButton)
    conf = ConfigParser()
    conf.read(perfil_vacio)
    assert conf['1B411']['teclas'] == 'Ctrl+Alt+H'
    assert conf['1B411']['codigo'] == '72'
    assert conf['1B411']['tipo'] == 'Pulsar'
    assert conf['1B411']['punto'] == 'sp87'


def test_rotatorios(mocker, captura, qtbot, perfil_vacio):
    """ Comprueba que al cabiar el ckeckbox de los rotatorios se actualiza
    el estado en el archivo de perfil y en las variables correspondientes
    del formulario principal.

    :param captura: instancia de frm_captura.
    :type captura: pytest fixture
    :param qtbot: Permite interactuar con los elementos gráficos.
    :type qtbot: pytest fixture
    :param mocker: permite acceso a funciones de mocking. 
    :type mocker: pytest-mock fixture
    :param perfil_vacio: ruta a un archivo de perfil vacío.
    :type perfil_vacio: pytest fixture
    """

    mock_parent = mocker.patch('frm_main.MainDialog', autospec=True)
    mock_instance = mock_parent.return_value
    mock_instance.modo_av_ret_rotatorios = False
    mock_instance.archivo_perfil = perfil_vacio
    parent = frm_main.MainDialog(None)
    captura.parent = parent
    conf = ConfigParser()
    conf.read(perfil_vacio)
    assert conf['ROTATORIOS']['Modo_av/ret'] == 'Desactivado'
    assert not captura.chkRotatorios.isChecked()
    assert not captura.parent.modo_av_ret_rotatorios
    qtbot.mouseClick(captura.chkRotatorios, QtCore.Qt.LeftButton)
    assert captura.chkRotatorios.isChecked()
    assert captura.parent.modo_av_ret_rotatorios
    conf.read(perfil_vacio)
    assert conf['ROTATORIOS']['Modo_av/ret'] == 'Activado'
