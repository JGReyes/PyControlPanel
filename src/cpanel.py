# -*- coding: utf-8 -*-
# File: cpanel.py
# Project: ControlPanelv3
# Created Date: Friday, January 5th 2018, 5:24:26 pm
# Author: JGReyes
# e-mail: josegpuntoreyes@gmail.com
# web: www.circuiteando.net
#
# ControlPanelv3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# ControlPanelv3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ControlPanelv3.  If not, see <http: //www.gnu.org/licenses/>.
#
# Copyright (c) 2018 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#

""" Contiene lo necesario para conectar y comunicarse por USB con el panel.
"""

import PySide.QtCore as qt
from time import sleep
from queue import Queue, Empty
import logging
import pywinusb.hid as hid
import time


class Cpanel(qt.QThread):
    """ Representa el panel de control.

        Se encarga de conectar por USB con el panel, manda y recibe los
        comandos al programa mediante dos colas:
        - entrada: se ponen los comandos a mandar al panel o el comando 0xcc
                   para terminar la conexión y los hilos de ejecución.
        - salida: contiene los comandos recibidos por el panel en formato
                  bytearray o las palabras 'Desconectado' y 'Conectado' para
                  informar cuando se conecta o desconecta el panel.
    """
    # Señal emitida cuando se pone un comando en la cola de salida.
    en_cola = qt.Signal()

    def __init__(self, parent=None):
        qt.QThread.__init__(self, parent)
        # Comandos a enviar al panel o al hilo del panel.
        self.entrada = Queue(5)
        # Comandos enviados por el panel.
        self.salida = Queue(5)
        self._vendor_id = 0x1234
        self._prouct_id = 0x1235
        self._panel = None
        self._conectado = False

    def run(self):
        ''' Inicia la conexión y se encarga de recibir datos.

        '''

        logging.basicConfig(filename='ControlPanelV3_Log.log',
                            level=logging.INFO,
                            format='%(asctime)s %(message)s',
                            datefmt='%m/%d/%Y %I:%M:%S %p',
                            filemode='w')

        while True:
            try:
                while not self._conectar_panel():
                    sleep(1)

                try:
                    datos = self.entrada.get_nowait()
                except Empty:
                    pass
                else:
                    if datos[0] == 0xcc:  # Termina la conexión.
                        logging.info('Terminado servicio USB.')
                        self._panel.close()
                        self.salida.put('Desconectado')
                        self.en_cola.emit()
                        return
                    elif datos[0] != 0:  # Manda los datos al panel.
                        self._enviar_hid(datos)
                sleep(0.1)
            except Exception as ex:
                logging.exception(''.join(['Error[Cpanel]: ', repr(ex)]))
                self._panel.close()

    def stop(self):
        """ Termina con la conexión e hilo de ejecucición del panel.
        """

        cmd_final = bytearray(6)
        cmd_final[0] = 0xcc
        self.entrada.put(cmd_final)

    def _conectar_panel(self):
        """ Realiza la conexión USB con el panel.

        :return: True si se ha conectado, en caso contrario False.
        :rtype: bool
        """

        try:
            if self._panel.is_plugged() and self._conectado:
                return True
        except Exception:   # Si is_plugged no existe todavía.
            pass

        filtro = hid.HidDeviceFilter(vendor_id=self._vendor_id,
                                     product_id=self._prouct_id)
        dispositivo_hid = filtro.get_devices()
        if dispositivo_hid:
            self._panel = dispositivo_hid[0]
            self._panel.set_raw_data_handler(self._leer_hid)
            self._panel.open()
            logging.info('Panel USB detectado.')
            self.salida.put('Conectado')
            self._conectado = True
            self.en_cola.emit()
            return True
        else:
            logging.info('Panel USB NO detectado.')
            self.salida.put('Desconectado')
            self._conectado = False
            self.en_cola.emit()
            return False

    def _enviar_hid(self, datos):
        """ Envía datos por USB.

        :param datos: Datos a mandar. (6 bytes como máximo)
        :type datos: list or tuple or bytearray
        """

        informe = self._panel.find_output_reports()
        buffer = bytearray(9)  # El informe hid necesita 9 bytes.

        for i in range(0, 6):
            buffer[i + 1] = datos[i]

        informe[0].set_raw_data(buffer)
        informe[0].send()

    def _leer_hid(self, datos):
        """ Lee los datos del panel y los coloca en la cola de salida.

        :param datos: Datos recibidos.
        :type datos: bytearray
        ..note:: 
            Se llama automáticamente a esta función al recibir los datos.
        """

        buffer = bytearray(6)
        for i in range(0, 6):
            buffer[i] = datos[i + 1]

        # Solo los pone en cola si el primer elemento no es 0.
        if buffer[0] != 0x00:
            self.salida.put(buffer)
            self.en_cola.emit()
