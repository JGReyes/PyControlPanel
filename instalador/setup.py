# Crea el directorio build con los archivos necesarios para ejecutar la
# aplicación.

import sys
from cx_Freeze import setup, Executable

build_exe_options = {
    "packages": ["atexit", "PySide.QtCore", "PySide.QtGui", "pywinusb.hid", "crcmod"],
    "include_files": [],
    "includes": ["captura", "mainwindow", "images_rc"],
    "path": sys.path + ['gui'],
    "excludes": [],
    "optimize": 2,
    "include_msvcr": 1

}

executable = [
    Executable("../src/main.py",
               base="Win32GUI",
               targetName="PyControlPanel.exe",
               icon="../gui/img/icono.ico")
]

setup(
    name="PyControlPanelV3",
    version="1.0",
    author="JGReyes",
    description="Programa para controlar el panel v3.",
    options={"build_exe": build_exe_options},
    executables=executable)
