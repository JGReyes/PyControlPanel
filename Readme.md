El programa de PC realizado en Python 3 que se encarga de recibir los datos del panel de control y emular un teclado. Pudiendo crear/lanzar distintos perfiles.

![Programa_PC](https://4.bp.blogspot.com/-NYZ_7It-lyo/WzURjjs73SI/AAAAAAAABLE/CtZDlGpFKgM2lQ2I1FqoyeQB17nfVNlFQCKgBGAs/s1600/captura.jpg)

Las características del programa son las siguientes:

- Realiza comprobación de errores (CRC 16) en la comunicación.
- Emula secuencias de teclas mediante scancodes.
- Capturar pulsaciones del teclado y traducirlas a scancodes.
- Crear y modificar perfiles de teclas.
- Cargar automáticamente el último perfil utilizado.
- Informar del estado actual del panel y modo de ejecución.
- Poder probar los perfiles y controles para asegurar su correcto funcionamiento.
- Recuperación de errores mediante reinicio automático de procesos.
- Lectura y envío de informes USB HID.

A diferencia de la versión anterior en FreePascal, en esta versión se han incorporado tests para comprobar que todo va bien después hacer cambios, incluyendo mocks para probar los métodos sin necesidad de tener el panel conectado físicamente y se ha utilizado la biblioteca hyperbole para generar de forma aleatoria números y texto para probar los parámetros de ciertos métodos.

Para más información consultar el [blog.](https://www.circuiteando.net/2018/06/pycontrolpanel.html)
