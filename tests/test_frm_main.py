# -*- coding: utf-8 -*-
# File: test_frm_main.py
# Project: ControlPanelv3
# Created Date: Friday, January 26th 2018, 1:18:13 pm
# Author: JGReyes
# e-mail: josegpuntoreyes@gmail.com
# web: www.circuiteando.net
#
# ControlPanelv3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# ControlPanelv3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ControlPanelv3.  If not, see <http: //www.gnu.org/licenses/>.
#
# Copyright (c) 2018 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#

""" Test para frm_main.py
"""

from configparser import ConfigParser
from pathlib import PureWindowsPath as winpath
import os
import time
import pytest
import frm_captura
import frm_main
from pytestqt import qtbot
from PySide import QtCore, QtGui
import winreg


@pytest.fixture
def main(qtbot):
    """ Crea una instancia de frm_main para realizar los tests.

    :param qtbot: Permite interactuar con los elementos gráficos.
    :type qtbot: pytest fixture
    :return: instancia de frm_main
    :rtype: objeto
    """
    if os.path.exists(os.getcwd() + '\perfil_inicio.ini'):
        os.remove(os.getcwd() + '\perfil_inicio.ini')

    main = frm_main.MainDialog(None)
    main.show()
    qtbot.addWidget(main)
    return main


@pytest.fixture
def archivo_perfil(tmpdir):
    """ Crea un archivo de perfil en un directorio temporal.

    :param tmpdir: permite trabajar con carpetas temporales.
    :type tmpdir: pytest fixture
    :return: ruta a un archivo de perfil.
    :rtype: string
    """

    config_file = tmpdir.mkdir('config').join('config.cp3')
    conf = ConfigParser()
    conf.add_section('ROTATORIOS')
    conf.set('ROTATORIOS', 'Modo_av/ret', 'Desactivado')
    conf.add_section('1B411')
    conf.set('1B411', 'teclas', 'Ctrl+Alt+H')
    conf.set('1B411', 'codigo', '72')
    conf.set('1B411', 'tipo', 'Pulsar')
    conf.set('1B411', 'punto', 'sp87')
    with open(str(config_file), 'w') as configfile:
        conf.write(configfile)
    return str(config_file)


def test_valores_iniciales(main):
    """ Comprueba que los valores de los atributos son los correctos
    al iniciar el formulario.

    :param main: instancia del formulario frm_main.
    :type main: pytest fixture
    """

    assert not main.archivo_perfil
    assert not main.modo_av_ret_rotatorios
    assert main._perfil is None
    assert not main.capturar_puntos
    assert main.modo_actual == frm_main.Modo.normal
    assert main.panel_activo
    assert main._pos_rot_a == 0xA0
    assert main._pos_rot_c == 0xC0
    assert main._pos_rot_d == 0xD0
    assert main._cp3_usb is None
    assert isinstance(main.fcaptura, frm_captura.CapturaDialog)
    assert main.lblModo_2.text() == 'Normal'
    assert main.lblCon_2.text() == 'Desconec.'


def test_btn_carga_perfil(main, qtbot, archivo_perfil, mocker):
    """ Comprueba la lógica del botón de carga de perfil.

    :param main: instancia del formulario frm_main.
    :type main: pytest fixture
    :param qtbot: Permite interactuar con los elementos gráficos.
    :type qtbot: pytest fixture
    :param mocker: permite acceso a funciones de mocking.
    :type mocker: pytest-mock fixture
    :param archivo_perfil: ruta a un archivo de perfil.
    :type archivo_perfil: pytest fixture
    """
    mock_mbox = mocker.patch('main.QtGui.QMessageBox', autospec=True)
    mock_mbox.return_value.exec_.return_value = QtGui.QMessageBox.Yes
    mock_file_dialog = mocker.patch('main.QtGui.QFileDialog', autospec=True)
    mock_file_dialog.return_value.selectedFiles.return_value = [
        archivo_perfil, ]
    mock_crear_perfil = mocker.patch.object(
        main, 'crear_perfil', autospec=True)
    mock_cargar_perfil = mocker.patch.object(
        main, 'cargar_perfil', autospec=True)
    qtbot.mouseClick(main.btnCargar, QtCore.Qt.LeftButton)
    # Si se elige un archivo existente.
    mock_cargar_perfil.assert_called_with(archivo_perfil)
    mock_file_dialog.return_value.selectedFiles.return_value = [
        'Archivo_inexistente.cp3', ]
    # Si se no existe el archivo y se quiere crear uno nuevo.
    qtbot.mouseClick(main.btnCargar, QtCore.Qt.LeftButton)
    mock_crear_perfil.assert_called_with('Archivo_inexistente.cp3')
    # Si se no existe el archivo y NO se quiere crear uno nuevo.
    mock_mbox.return_value.exec_.return_value = QtGui.QMessageBox.No
    qtbot.mouseClick(main.btnCargar, QtCore.Qt.LeftButton)
    assert mock_crear_perfil.call_count == 1
    assert mock_cargar_perfil.call_count == 1


def test_puntos(main):
    """ Comprueba que los puntos se muestran y se ocultan en pantalla.

    :param main: instancia del formulario frm_main.
    :type main: pytest fixture
    """

    main.mostrar_puntos()
    assert main.lbl_sp93.isVisible()
    main.mostrar_puntos(False)
    assert main.lbl_sp93.isHidden()


def test_perfil_por_def_reg(main, mocker, archivo_perfil):
    """ Comprueba que lee y crea la ruta al perfil de inicio
    en el registro de windows.

    :param main: instancia del formulario frm_main.
    :type main: pytest fixture
    :param mocker: permite acceso a funciones de mocking.
    :type mocker: pytest-mock fixture
    :param tmpdir: permite trabajar con carpetas temporales.
    :type tmpdir: pytest fixture
    """

    mock_cargar = mocker.patch.object(main, 'cargar_perfil', autospec=True)
    program_key = winreg.CreateKey(winreg.HKEY_CURRENT_USER,
                                   'Software\PyControlPanelv3')
    winreg.SetValueEx(program_key, 'perfil_defecto', 0,
                      winreg.REG_SZ, archivo_perfil)
    winreg.FlushKey(program_key)

    # Con un registro de perfil de inicio y fichero de perfil.
    main.perfil_por_def_reg(True)
    mock_cargar.assert_called_with(archivo_perfil)

    # Con un registro de perfil de inicio y sin fichero de perfil.
    mock_cargar.reset_mock()
    os.remove(archivo_perfil)
    assert not os.path.isfile(archivo_perfil)
    main.perfil_por_def_reg(True)
    assert not mock_cargar.called

    # Sin registro, se pide la creacion de uno nuevo.
    winreg.DeleteValue(program_key, 'perfil_defecto')
    winreg.FlushKey(program_key)
    main.perfil_por_def_reg(False, 'c:\\ruta\perfil.cp3')
    ruta_perfil_def, tipo_reg = winreg.QueryValueEx(program_key,
                                                    'perfil_defecto')
    assert ruta_perfil_def == 'c:\\ruta\perfil.cp3'
    # Se borra el perfil por defecto.
    winreg.DeleteValue(program_key, 'perfil_defecto')
    winreg.CloseKey(program_key)


@pytest.mark.skip(reason='Se utiliza la versión del registro de windows, \
                          utilizar éste test si se cambia por la del archivo \
                          .ini')
def test_perfil_por_def(main, mocker, tmpdir):
    """ Comprueba que carga y crea un el archivo de perfil de inicio.

    :param main: instancia del formulario frm_main.
    :type main: pytest fixture
    :param mocker: permite acceso a funciones de mocking.
    :type mocker: pytest-mock fixture
    :param tmpdir: permite trabajar con carpetas temporales.
    :type tmpdir: pytest fixture
    """

    perfil_inicio = tmpdir.join('perfil_inicio.ini')
    mocker.patch.object(main, 'cargar_perfil', autospec=True)
    mock_getcwd = mocker.patch('os.getcwd')
    mock_getcwd.return_value = perfil_inicio.dirname
    config = ConfigParser()
    config.add_section('PERFIL')
    config.set('PERFIL', 'Por_defecto', 'c:\perfil.cp3')
    with open(str(perfil_inicio), 'w') as perfilfile:
        config.write(perfilfile)

    # Con un archivo de perfil de inicio..
    main.perfil_por_def(True)
    assert main.cargar_perfil.called

    # Sin archivo, se pide la creacion de uno nuevo.
    perfil_inicio.remove()
    assert not os.path.isfile(str(perfil_inicio))
    main.perfil_por_def(False, 'c:\\ruta\perfil.cp3')
    config.read(str(perfil_inicio))
    assert config['PERFIL']['Por_defecto'] == 'c:\\ruta\perfil.cp3'


def test_teclado_pulsar(main, archivo_perfil, mocker):
    """ Comprueba que cuando se pulsa un tecla del panel, se procede acorde
    al modo del programa.

    :param main: instancia del formulario frm_main.
    :type main: pytest fixture
    :param archivo_perfil: ruta a un archivo de perfil.
    :type archivo_perfil: pytest fixture
    :param mocker: permite acceso a funciones de mocking.
    :type mocker: pytest-mock fixture
    """

    main.cargar_perfil(archivo_perfil)
    assert isinstance(main._perfil, ConfigParser)
    assert main.panel_activo
    main.teclado_pulsar('0B460')
    assert not main.panel_activo
    assert main.teclado_pulsar('2D511') == 1
    main.teclado_pulsar('0B460')
    assert main.panel_activo
    assert main.teclado_pulsar('0B461') == 1
    assert main.teclado_pulsar('000E7') == 1
    # Test en modo normal
    mock_ejecutar_teclas = mocker.patch.object(
        main, 'ejecutar_teclas', autospec=True)
    main.teclado_pulsar('1B411')
    mock_ejecutar_teclas.assert_called_with('Pulsar', 'Ctrl+Alt+H', 72)
    # Test en modo edición
    main.modo_actual = frm_main.Modo.edicion
    assert main.teclado_pulsar('1B411') == 1
    time.sleep(0.1)
    assert main.teclado_pulsar('1B411') is None
    assert main.fcaptura.comando == '1B411'


def test_cargar_teclas(main, archivo_perfil):
    """ Comprueba que se carga la configuración del comando a emular y que
    se muestre en pantalla si así se desea.

    :param main: instancia del formulario frm_main.
    :type main: pytest fixture
    :param archivo_perfil: ruta a un archivo de perfil.
    :type archivo_perfil: pytest fixture
    """
    main.cargar_perfil(archivo_perfil)
    assert isinstance(main._perfil, ConfigParser)
    res = main.cargar_teclas('1B411')
    assert res == ('Pulsar', 'sp87', 'Ctrl+Alt+H', 72)
    res = main.cargar_teclas('1FFFF')
    assert res == ()
    main.cargar_teclas('1B411', True)
    assert main.fcaptura.cmbTipo.currentText() == 'Pulsar'
    assert main.fcaptura.lblPunto2.text() == 'sp87'
    assert main.fcaptura.lblTeclas2.text() == 'Ctrl+Alt+H'
    assert main.fcaptura.lblCodigo.text() == '72'


def test_ejecutar_teclas(main, mocker):
    """ Comprueba que se llama a los metodos de pulsar y soltar tecla
    del módulo 'teclado' y que el número de llamadas coincide con la secuencia
    de teclas enviada.

    :param main: instancia del formulario frm_main.
    :type main: pytest fixture
    :param mocker: permite acceso a funciones de mocking.
    :type mocker: pytest-mock fixture
    """

    mock_teclado = mocker.patch('teclado.Teclado', autospec=True)
    mock_inst_teclado = mock_teclado.return_value
    mock_inst_teclado.pulsar.return_value = True
    mock_inst_teclado.soltar.return_value = True

    main.ejecutar_teclas('Pulsar', 'Shift+Ctrl+Alt+H', 72)
    assert mock_inst_teclado.pulsar.call_count == 4
    assert mock_inst_teclado.soltar.call_count == 4


def test_modo_toggled(main, qtbot):
    """ Comprueba que al pulsar los botones de modo, se cambia el modo y se
    muestra en pantalla.

    :param main: instancia del formulario frm_main.
    :type main: pytest fixture
    :param qtbot: Permite interactuar con los elementos gráficos.
    :type qtbot: pytest fixture
    """

    qtbot.mouseClick(main.btnEdicion, QtCore.Qt.LeftButton)
    assert main.modo_actual == frm_main.Modo.edicion
    assert main.lblModo_2.text() == 'Edición'
    qtbot.mouseClick(main.btnPrueba, QtCore.Qt.LeftButton)
    assert main.modo_actual == frm_main.Modo.prueba
    assert main.lblModo_2.text() == 'Prueba'
    qtbot.mouseClick(main.btnNormal, QtCore.Qt.LeftButton)
    assert main.modo_actual == frm_main.Modo.normal
    assert main.lblModo_2.text() == 'Normal'


def test_conexion_toggled(main, qtbot, mocker):
    """ Comprueba que se lanzan los hilos de ejecución para el panel y
    la escucha de datos cuando se pulsa en On. Y que se manda detener
    los hilos al pulsar en Off

    :param main: instancia del formulario frm_main.
    :type main: pytest fixture
    :param qtbot: Permite interactuar con los elementos gráficos.
    :type qtbot: pytest fixture
    :param mocker: permite acceso a funciones de mocking.
    :type mocker: pytest-mock fixture
    """

    mock_panel = mocker.patch('cpanel.Cpanel')
    mock_panel.en_cola.connect.return_value = True
    qtbot.mouseClick(main.btnOn, QtCore.Qt.LeftButton)
    assert mock_panel.return_value.start.called
    qtbot.mouseClick(main.btnOff, QtCore.Qt.LeftButton)
    assert mock_panel.return_value.stop.called
    assert main._cp3_usb is None


def test_crc16():
    """ Comprueba que el crc de un byte y un comando conocido es correcto.

    """

    dato = 0xfe  # crc16/arc = 0x8081
    assert frm_main.MainDialog.crc16_update(dato, False) == 0x8081
    comando = int('1C530', 16)  # crc16/arc = 0x9F92
    cmd = comando.to_bytes(length=3, byteorder='little')
    assert frm_main.MainDialog.crc16_update(cmd) == 0x9F92


def test_cargar_perfil(main, archivo_perfil):
    """ Comprueba que se carga correctamente un perfil, dada la ruta al
    archivo.

    :param main: instancia del formulario frm_main.
    :type main: pytest fixture
    :param archivo_perfil: ruta a un archivo de perfil.
    :type archivo_perfil: pytest fixture
    """

    main.cargar_perfil(archivo_perfil)
    assert isinstance(main._perfil, ConfigParser)
    assert main.archivo_perfil == archivo_perfil
    assert main.lblPerfil_2.text() == winpath(archivo_perfil).name[:-4]


def test_crear_perfil(main, tmpdir, mocker):
    """[summary]

    :param main: instancia del formulario frm_main.
    :type main: pytest fixture
    :param tmpdir: permite trabajar con carpetas temporales.
    :type tmpdir: pytest fixture
    :param mocker: permite acceso a funciones de mocking.
    :type mocker: pytest-mock fixture
    """

    mock_cargar_perfil = mocker.patch.object(
        main, 'cargar_perfil', autospec=True)
    archivo = tmpdir.mkdir('perfil').join('perfilnuevo.cp3')
    main.crear_perfil(str(archivo))
    conf = ConfigParser()
    conf.read(str(archivo))
    assert conf['ROTATORIOS']['Modo_av/ret'] == 'Desactivado'
    assert len(conf.sections()) == 1
    mock_cargar_perfil.assert_called_with(str(archivo))


def test_preparar_trama(main, mocker):
    """ Comprueba que se prepara correctamente la trama de un comando dado para
    enviarlo al panel.

    :param main: instancia del formulario frm_main.
    :type main: pytest fixture
    :param mocker: permite acceso a funciones de mocking.
    :type mocker: pytest-mock fixture
    """

    mock_main = mocker.patch('frm_main.MainDialog', autospec=True)
    mock_main.crc16_update.return_value = 7
    trama = main.preparar_trama(int('1B411', 16))
    assert trama == b'\x7e\x01\xb4\x11\x00\x07'


def test_leer_trama(main, mocker):
    """ Comprueba que se lee correctamente una trama enviada por el panel,
    o se devuelva 0 en caso de no ser válida.

    :param main: instancia del formulario frm_main.
    :type main: pytest fixture
    :param mocker: permite acceso a funciones de mocking.
    :type mocker: pytest-mock fixture
    """

    mock_main = mocker.patch('frm_main.MainDialog', autospec=True)
    mock_main.crc16_update.return_value = 7
    # Trama válida
    trama = b'\x7e\x01\xb4\x11\x00\x07'
    assert main.leer_trama(trama) == int('1B411', 16)
    # Trama sin byte de sincronización.
    trama = b'\x11\x01\xb4\x11\x00\x07'
    assert main.leer_trama(trama) == 0
    # Trama con crc erróneo
    trama = b'\x7e\x01\xb4\x11\x00\x08'
    assert main.leer_trama(trama) == 0


def test_comprueba_datos_usb(main, mocker):
    """ Comprueba la lógica de la función de recepción de datos por usb.

    :param main: instancia del formulario frm_main.
    :type main: pytest fixture
    :param mocker: permite acceso a funciones de mocking.
    :type mocker: pytest-mock fixture
    """

    trama = bytearray(b'\x7e\x01\xb4\x11\x00\x07')
    mock_panel = mocker.patch('cpanel.Cpanel')
    mock_inst_panel = mock_panel.return_value
    mock_inst_panel.salida.get.return_value = trama
    main._cp3_usb = mock_inst_panel
    mock_leer = mocker.patch.object(main, 'leer_trama', autospec=True)
    mock_leer.return_value = int('1B411', 16)
    mock_preparar = mocker.patch.object(main, 'preparar_trama', autospec=True)
    mock_preparar.return_value = bytearray(b'\x7e\xe7\x00\x00\x00\x00')
    mock_pulsar = mocker.patch.object(main, 'teclado_pulsar', autospec=True)
    # trama válida
    main.comprueba_datos_usb()
    mock_leer.assert_called_with(trama)
    mock_inst_panel.entrada.put.assert_called_with(b'\xaa\x00\x00\x00\x00\x00')
    mock_pulsar.assert_called_with('1B411')
    # trama no válida
    mock_leer.return_value = 0
    mock_pulsar.reset_mock()
    main.comprueba_datos_usb()
    mock_preparar.assert_called_with(0xe7)
    mock_inst_panel.entrada.put.assert_called_with(b'\x7e\xe7\x00\x00\x00\x00')
    assert not mock_pulsar.called
    # Trama = Desconectado
    mock_pulsar.reset_mock()
    mock_leer.reset_mock()
    mock_preparar.reset_mock()
    mock_inst_panel.entrada.put.reset_mock()
    mock_inst_panel.salida.get.return_value = 'Desconectado'
    main.comprueba_datos_usb()
    assert not mock_pulsar.called
    assert not mock_leer.called
    assert not mock_preparar.called
    assert not mock_inst_panel.entrada.put.called
    assert main.lblCon_2.text() == 'Desconec.'
    # Trama = Conectado
    mock_pulsar.reset_mock()
    mock_leer.reset_mock()
    mock_preparar.reset_mock()
    mock_inst_panel.entrada.put.reset_mock()
    mock_inst_panel.salida.get.return_value = 'Conectado'
    main.comprueba_datos_usb()
    assert not mock_pulsar.called
    assert not mock_leer.called
    assert not mock_preparar.called
    assert not mock_inst_panel.entrada.put.called
    assert main.lblCon_2.text() == 'Conectado'


def test_nombre_punto(main, qtbot):
    """ Comprueba que al hacer click en un punto rojo, se muestra su nombre en 
    el formulario de captura.

    :param main: instancia del formulario frm_main.
    :type main: pytest fixture
    :param qtbot: Permite interactuar con los elementos gráficos.
    :type qtbot: pytest fixture
    """

    main.modo_actual = frm_main.Modo.edicion
    main.fcaptura.show()
    qtbot.mouseClick(main.lbl_sp1, QtCore.Qt.LeftButton)
    assert main.fcaptura.lblPunto2.text() == 'sp1'
