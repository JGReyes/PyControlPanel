# -*- coding: utf-8 -*-
# File: frm_captura.py
# Project: ControlPanelv3
# Created Date: Friday, December 15th 2017, 6:16:15 pm
# Author: JGReyes
# e-mail: josegpuntoreyes@gmail.com
# web: www.circuiteando.net
#
# ControlPanelv3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# ControlPanelv3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ControlPanelv3.  If not, see <http: //www.gnu.org/licenses/>.
#
# Copyright (c) 2017 JGReyes
#
# ------------------------------------
# Last Modified: (INSERT DATE)         <--!!!!!!!!
# Modified By: JGReyes
# ------------------------------------
#
# Changes:
#
# Date       Version   Author      Detail
# (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
#

""" Se encarga de crear y editar perfiles de teclado para el panel de control.
"""

import configparser
from PySide import QtGui, QtCore
import captura
from teclado import Teclado


class CapturaDialog(QtGui.QDialog, captura.Ui_captura):
    """ Muestra el formulario de captura y realiza la creación y edición
    de perfiles.
    """

    def __init__(self, parent=None):
        super(CapturaDialog, self).__init__(parent)
        self.setupUi(self)
        self.parent = parent
        # Almacena el comando capturado por el formulario principal.
        self.comando = ''
        self.capturar = False  # Indica si se deben capturar las teclas.
        # Indica si se ha introducido el código de un tecla.
        self.codigo_intro = False
        self.codigos = []
        self.btnCapturar.installEventFilter(self)
        self.installEventFilter(self)
        self.cmbTipo.addItems(['Pulsar', 'Mantener', 'Soltar'])

    def btn_capturar_click(self):
        """ Inicia la captura de teclas.
        """

        if not self.capturar:
            self.capturar = True
            self.btnCapturar.setText('Capturando...')
            self.lblTeclas2.setText('')
            self.lblCodigo.setText('')
            self.codigo_intro = False
            self.codigos = []

        else:
            self.capturar = False
            self.btnCapturar.setText('Iniciar/parar captura')

    def eventFilter(self, widget, event):
        """ Captura los eventos de la aplicación para llamar a la función
        adecuada.

        :param widget: Elemento gráfico donde sucedió el evento
        :type widget: QtQui widget
        :param event: Evento que ha sucedido
        :type event: Qt event
        :return: El evento que no interesa se propaga.
        :rtype: Qt event
        """

        if (self.capturar and event.type() == QtCore.QEvent.KeyPress and
                widget is self.btnCapturar):
            self.btn_capturar_keypress(event)
            return True

        if (self.capturar and event.type() == QtCore.QEvent.MouseButtonPress
                and widget is self.btnCapturar):
            self.btn_capturar_mouseclick(event)

        if event.type() == QtCore.QEvent.Show and widget is self:
            self.parent.mostrar_puntos()

        if event.type() == QtCore.QEvent.Close and widget is self:
            self.parent.mostrar_puntos(False)

        return QtGui.QWidget.eventFilter(self, widget, event)

    def btn_capturar_keypress(self, tecla):
        """ Captura las teclas y las muestra en pantalla.

        :param tecla: Contiene la tecla pulsada.
        :type tecla: Qt event
        """

        modificadores = (QtCore.Qt.Key_Shift, QtCore.Qt.Key_Control,
                         QtCore.Qt.Key_Alt)

        tec = tecla.nativeVirtualKey()

        if tec in Teclado.teclas and tec not in self.codigos:
            # Solo se añade la tecla si no está repetida.
            if not self.codigo_intro:
                self.codigos.append(tec)
                self.lblTeclas2.setText('+'.join([self.lblTeclas2.text(),
                                                  Teclado.teclas[tec][0]]))
                texto = self.lblTeclas2.text()
                if texto[0] == '+':
                    self.lblTeclas2.setText(texto[1:])
                if tecla.key() not in modificadores:
                    self.codigo_intro = True
                    self.lblCodigo.setText(str(tec))
                    self.btnCapturar.click()

    def btn_capturar_mouseclick(self, mouse):
        """ Permite introducir la tecla enter del teclado numérico, ya que en
        los teclados sin numpad, hay que recurrir al teclado virtual de windows
        y en éste la tecla enter del numérico comparte el código con el enter
        del teclado.

        :param mouse: Evento de ratón.
        :type mouse: Qt event
        """

        if QtCore.Qt.RightButton == mouse.button():
            if not self.codigo_intro:
                tecla = QtGui.QKeyEvent.createExtendedKeyEvent(
                    QtCore.QEvent.KeyPress,
                    QtCore.Qt.Key_Enter,
                    QtCore.Qt.KeypadModifier, 0, 176, 0)
                self.btn_capturar_keypress(tecla)

    def btn_aceptar_click(self):
        """ Se encarga de guardar las teclas capturadas en el archivo de perfil
        y de cerrar el formulario.
        """

        if self.capturar:
            self.btnCapturar.click()

        try:
            config = configparser.ConfigParser()
            config.read(self.parent.archivo_perfil)
            if not config.has_section(self.comando):
                config.add_section(self.comando)

            config.set(self.comando, 'teclas', self.lblTeclas2.text())
            config.set(self.comando, 'codigo', self.lblCodigo.text())
            config.set(self.comando, 'tipo', self.cmbTipo.currentText())
            config.set(self.comando, 'punto', self.lblPunto2.text())

            with open(self.parent.archivo_perfil, 'w') as configfile:
                config.write(configfile)

            self.parent.cargar_perfil(self.parent.archivo_perfil)
            self.close()
        except Exception as ex:
            mbox = QtGui.QMessageBox(QtGui.QMessageBox.Critical,
                                     'Se ha producido un error',
                                     repr(ex), QtGui.QMessageBox.Ok, self)
            mbox.exec_()

    def chk_rotatorios_changed(self):
        """ Al cambiar el estado del checkbox, se actualiza el valor de la
        variable que indica si está o no el modo av/ret de los rotatorios
        activado. También guarda el valor en el archivo de perfil.
        """

        try:
            config = configparser.ConfigParser()
            config.read(self.parent.archivo_perfil)

            if self.chkRotatorios.isChecked():
                config['ROTATORIOS']['Modo_av/ret'] = 'Activado'
                self.parent.modo_av_ret_rotatorios = True
            else:
                config['ROTATORIOS']['Modo_av/ret'] = 'Desactivado'
                self.parent.modo_av_ret_rotatorios = False

            with open(self.parent.archivo_perfil, 'w') as configfile:
                config.write(configfile)

        except Exception as ex:
            mbox = QtGui.QMessageBox(QtGui.QMessageBox.Critical,
                                     'Se ha producido un error',
                                     repr(ex), QtGui.QMessageBox.Ok, self)
            mbox.exec_()
